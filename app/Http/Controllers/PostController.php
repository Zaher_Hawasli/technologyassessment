<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Validator;
use App\Jobs\SendEmailJob;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $posts = Post::all();
        return response()->json($posts);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'post_type' => 'required|in:1,2',
            'user_id'=>'required'
        ])->validate(); 

        // details , video file validation
        if($request->post_type==1){
            $validator = Validator::make($request->all(), [
                'details' => 'required',
            ])->validate(); 
        }else{
            $validator = Validator::make($request->all(), [
                'file' => 'required',
            ])->validate();
        }

        if($request->hasFile('file')){
            $file_path=Post::UploadPostVideo($request);
            $post = Post::create(array_merge($request->all(),['video_path'=>$file_path]));
        }else{
            $post = Post::create($request->all());
        }
        
        // send email to users
        $users=User::all();
        $details = [
            'title'   => 'New Post notification',
            "content" =>'Hi, Sir
            Hello there\'s a intersted article here.'
        ];
        foreach ($users as $user) {
            $email=$user->email;
            $details['email']=$email;
            dispatch(new SendEmailJob($details));
        }
        // send email to users
        

        return response()->json([
            'message'=>'Post Created Successfully!!',
            'post'=>$post
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Post $post)
    {
        $post->load('Comment');
        return response()->json($post);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Post $post)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'post_type' => 'required|in:1,2',
            'user_id'=>'required'
        ])->validate(); 

        // details , video file validation
        if($request->post_type==1){
            $validator = Validator::make($request->all(), [
                'details' => 'required',
            ])->validate(); 
        }else{
            $validator = Validator::make($request->all(), [
                'file' => 'required',
            ])->validate();
        }

        if($request->hasFile('file')){
            File::delete(public_path($post->video_path));
            $file_path=Post::UploadPostVideo($request);
            $post->update(array_merge($request->all(),['video_path'=>$file_path,'details'=>'']));
        }else{
            $post->update($request->all());
        }

        return response()->json([
            'message'=>'Post Updated Successfully!!',
            'post'=>$post
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post)
    {
        File::delete(public_path($post->video_path));
        $post->delete();
        return response()->json([
            'message'=>'Post Deleted Successfully!!'
        ]);
    }
}
