<?php

namespace App\Http\Controllers;

use App\Models\PostComment;
use Illuminate\Http\Request;
use Validator;

class PostCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return $comments= PostComment::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
            'post_id' => 'required',
            'user_id'=>'required'
        ])->validate(); 

        $comment = PostComment::create($request->all());
        return response()->json([
            'message'=>'Comment Created Successfully!!',
            'Comment'=>$comment
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(PostComment $postComment)
    {
        return response()->json($postComment);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PostComment $postComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PostComment $postComment)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
        ])->validate(); 
        
        $postComment->update($request->all());
        return response()->json([
            'message'=>'Comment Updated Successfully!!',
            'Comment'=>$postComment
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PostComment $postComment)
    {
        $postComment->delete();
        return response()->json([
            'message'=>'Comment Deleted Successfully!!'
        ]);
    }
}
