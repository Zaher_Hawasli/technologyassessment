<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = ['title','user_id','post_type','details','video_path'];

    public function Comment(){
        return $this->hasMany(PostComment::class);
    }

    // move video to videos folder and if not exist will be created by mkdir function
    public static function UploadPostVideo($request){
        $file = $request->file('file');
        //video name
            $name=$request->title.'_'.time().'.'.$file->getClientOriginalExtension();
            if(!is_dir(public_path('Videos')))
            mkdir(public_path('Videos'), 0755);
            $file_path ='Videos/'. $name;
            $dest = public_path('Videos');
            $file->move($dest, $name);
            return $file_path;
    }
}
