<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PostCommentController;
use App\Http\Controllers\PassportController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('register', [PassportController::class, 'register']);
Route::post('login', [PassportController::class, 'login']);


// put all api protected routes here
Route::middleware('auth:api')->group(function () {
    
    Route::post('get_user_details', [PassportController::class, 'user_details']);
    Route::post('logout', [PassportController::class, 'logout']);

    //post routes
    Route::apiResource('posts', PostController::class);


    //postComments routes
    Route::apiResource('postComments', PostCommentController::class);
});


